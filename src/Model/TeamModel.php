<?php 

namespace App\Model;

use App\Entity\TeamEntity;
use App\Service\Database;

/**
 * @property string $table
 */
class TeamModel extends Database {

    /** @var string */
    private $table = 'team';

    /**
     * Récupère toutes les équipes
     * 
     * @return array|false
     */
    public function findAll()
    {
        $query = $this->getPDO()->prepare("SELECT * FROM $this->table");
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_CLASS, TeamEntity::class);
    }
}