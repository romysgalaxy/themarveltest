<?php 

namespace App\Model;

use App\Entity\HeroEntity;
use App\Service\Database;

/**
 * @property string $table
 */
class HeroPowerModel extends Database {

    /** @var string */
    private $table = 'hero_power';

    /**
     * Récupère les compétences d'un héro
     * 
     * @param int $id hero.id
     * @return array|false
     */
    public function findPower(int $id)
    {
        $query = $this->getPDO()->prepare(
            'SELECT * FROM '  . $this->table . ' LEFT JOIN power on ' . $this->table . '.power_id = power.id LEFT JOIN hero on ' . $this->table . '.hero_id = hero.id WHERE hero.id = ' . $id   
        );
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_CLASS, HeroEntity::class);
    }
}