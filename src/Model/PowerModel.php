<?php 

namespace App\Model;

use App\Entity\PowerEntity;
use App\Service\Database;

/**
 * @property string $table
 */
class PowerModel extends Database {

    /** @var string */
    private $table = 'power';

    /**
     * Récupère tous les super-pouvoirs
     * 
     * @return array|false
     */
    public function findAll()
    {
        $query = $this->getPDO()->prepare("SELECT * FROM $this->table");
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_CLASS, PowerEntity::class);
    }
}