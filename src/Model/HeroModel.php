<?php 

namespace App\Model;

use App\Entity\HeroEntity;
use App\Service\Database;

/**
 * @property string $table
 */
class HeroModel extends Database {

    /** @var string */
    private $table = 'hero';

    /**
     * Récupère un héros
     * 
     * @return HeroEntity|false
     */
    public function findOne(int $id)
    {
        $query = $this->getPDO()->prepare('SELECT * FROM '  . $this->table . ' WHERE id = ' . $id);
        $query->execute();

        $result = $query->fetchAll(\PDO::FETCH_CLASS, HeroEntity::class);

        return reset($result);
    }

    /**
     * Récupère tous les héros
     * 
     * @return array|false
     */
    public function findAll()
    {
        $query = $this->getPDO()->prepare('SELECT * FROM '  . $this->table);
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_CLASS, HeroEntity::class);
    }

    /**
     * Filtre les héros par équipe
     * 
     * @param int $id
     * @return array|false
     */
    public function findByTeam(int $id)
    {
        $query = $this->getPDO()->prepare('SELECT * FROM '  . $this->table . ' WHERE team = ' . $id);
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_CLASS, HeroEntity::class);
    }

    /**
     * Création d'un héros
     * 
     * @param string $lastname hero.lastname
     * @param string $firstname hero.firstname
     * @param string $identity hero.identity
     * @param string $picture hero.picture
     * @param string $origin hero.origin
     * @param string $description hero.description
     * @param string $team hero.team
     * @return bool
     */
    public function create(
        string $lastname,
        string $firstname,
        string $identity,
        string $picture,
        string $origin,
        string $description,
        int $team
    ): bool
    {
        $query = $this->getPDO()->prepare("
        INSERT INTO $this->table (lastname, firstname, identity, picture, origin, description, team) 
        VALUES ('$lastname', '$firstname', '$identity', '$picture', '$origin', '$description', '$team')");
        return $query->execute();
    }

    /**
     * Mise à jour de l'attribut d'un héros
     * 
     * @param int $id hero.id
     * @param string $column
     * @param mixed $value
     * @return bool
     */
    public function update(int $id, string $column, mixed $value): bool
    {
        $query = $this->getPDO()->prepare("UPDATE $this->table SET $column = '$value' WHERE id = $id;");
        return $query->execute();
    }

    /**
     * Supression d'un héros
     * 
     * @param int $id hero.id
     * @return bool
     */
    public function delete(int $id): bool
    {
        // Récupère l'instance du héros pour récupérer le lien de stockage de l'image
        $heroEntity = $this->findOne($id);
        $picturePath = $heroEntity->getPicture();
        // Détruit l'image
        unlink(SERVER . 'asset/img/hero/' . $picturePath);
        // Procède à la suppression dans la base de donnée
        $query = $this->getPDO()->prepare("DELETE FROM $this->table WHERE id = $id");
        return $query->execute();
    }
}