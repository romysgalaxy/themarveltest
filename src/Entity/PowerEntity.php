<?php 

namespace App\Entity;

/**
 * @property int $id power.id
 * @property string $power power.power
 */
class PowerEntity {
    
    /** @var int */
    private $id;

    /** @var string */
    private $power;

    /**
     * Défini l'ID de l'entité
     * 
     * @param int $id
     * @return PowerEntity
     */
    public function setId(int $id): PowerEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Récupère l'ID de l'entité
     * 
     * @return int
     */
    public function getId(): int
    {
        return $this->id;   
    }

    /**
     * Défini le pouvoir de l'entité
     * 
     * @param string $power
     * @return PowerEntity
     */
    public function setPower(string $power): PowerEntity
    {
        $this->power = $power;
        return $this;   
    }

    /**
     * Récupère le pouvoir de l'entité
     * 
     * @return string
     */
    public function getPower(): string
    {
        return $this->power;   
    }
}