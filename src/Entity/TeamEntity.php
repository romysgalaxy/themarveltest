<?php 

namespace App\Entity;

/**
 * @property int $id team.id
 * @property string $name team.name
 */
class TeamEntity {
    
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /**
     * Défini l'ID de l'entité
     * 
     * @param int $id
     * @return TeamEntity
     */
    public function setId(int $id): TeamEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Récupère l'ID de l'entité
     * 
     * @return int
     */
    public function getId(): int
    {
        return $this->id;   
    }

    /**
     * Défini le nom de l'équipe
     * 
     * @param string $name
     * @return TeamEntity
     */
    public function setName(string $name): TeamEntity
    {
        $this->name = $name;
        return $this;   
    }

    /**
     * Récupère le nom de l'équipe
     * 
     * @return string
     */
    public function getName(): string
    {
        return $this->name;   
    }
}