<?php

namespace App\Entity;

/**
 * @property int $id hero.id
 * @property string $lastname hero.lastname
 * @property string $firstname hero.firstname
 * @property string $identity hero.identity
 * @property string $picture hero.picture
 * @property string $origin hero.origin
 * @property string $description hero.description
 * @property int $team hero.team
 */
class HeroEntity
{
    /** @var int */
    private $id;

    /** @var string */
    private $lastname;

    /** @var string */
    private $firstname;

    /** @var string */
    private $identity;

    /** @var string */
    private $picture;

    /** @var string */
    private $origin;

    /** @var string */
    private $description;

    /** @var int */
    private $team;

    /**
     * Défini l'ID de l'entité
     * 
     * @param int $id
     * @return HeroEntity
     */
    public function setId(int $id): HeroEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Récupère l'ID de l'entité
     * 
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Défini le nom du héro
     * 
     * @param string $lastname
     * @return HeroEntity
     */
    public function setLastname(string $lastname): HeroEntity
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * Récupère le nom du héro
     * 
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * Défini le prénom du héro
     * 
     * @param string $firstname
     * @return HeroEntity
     */
    public function setFirstname(string $firstname): HeroEntity
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * Récupère le prénom du héro
     * 
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * Défini l'identité du héro
     * 
     * @param string $identity
     * @return HeroEntity
     */
    public function setIdentity(string $identity): HeroEntity
    {
        $this->identity = $identity;
        return $this;
    }

    /**
     * Récupère l'identité du héro
     * 
     * @return string
     */
    public function getIdentity(): string
    {
        return $this->identity;
    }

    /**
     * Défini l'image du héro
     * 
     * @param string $picture
     * @return HeroEntity
     */
    public function setPicture(string $picture): HeroEntity
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * Récupère l'image du héro
     * 
     * @return string
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * Défini l'origine du héro
     * 
     * @param string $origin
     * @return HeroEntity
     */
    public function setOrigin(string $origin): HeroEntity
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * Récupère l'origine du héro
     * 
     * @return string
     */
    public function getOrigin(): string
    {
        return $this->origin;
    }

    /**
     * Défini la description du héro
     * 
     * @param string $description
     * @return HeroEntity
     */
    public function setDescription(string $description): HeroEntity
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Récupère la description du héro
     * 
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Défini la team du héro
     * 
     * @param int $team
     * @return HeroEntity
     */
    public function setTeam(int $team): HeroEntity
    {
        $this->team = $team;
        return $this;
    }

    /**
     * Récupère la team du héro
     * 
     * @return string
     */
    public function getTeam(): string
    {
        switch ($this->team) {
            case 1:
                return 'Avengers';
            case 2:
                return 'Guardians of the Galaxy';            
            case 3:
                return 'X-men';
            default:
                return '';
        }
    }
}
