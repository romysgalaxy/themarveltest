<?php

namespace App\Entity;

/**
 * @property int $id hero_power.id
 * @property string $power_id hero_power.power_id
 * @property string $hero_id hero_power.hero_id
 */
class HeroPowerEntity
{
    /** @var int */
    private $id;

    /** @var int */
    private $power_id;

    /** @var int */
    private $hero_id;

    /**
     * Défini l'ID de l'entité
     * 
     * @param int $id
     * @return HeroEntity
     */
    public function setId(int $id): HeroPowerEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Récupère l'ID de l'entité
     * 
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Défini l'ID power
     * 
     * @param int $power_id
     * @return HeroEntity
     */
    public function setPowerId(int $power_id): HeroPowerEntity
    {
        $this->power_id = $power_id;
        return $this;
    }

    /**
     * Récupère l'ID power
     * 
     * @return int
     */
    public function getPowerId(): int
    {
        return $this->power_id;
    }

    /**
     * Défini l'ID du hero
     * 
     * @param int $hero_id
     * @return HeroEntity
     */
    public function setHeroId(int $hero_id): HeroPowerEntity
    {
        $this->hero_id = $hero_id;
        return $this;
    }

    /**
     * Récupère l'ID du hero
     * 
     * @return int
     */
    public function getHeroId(): int
    {
        return $this->hero_id;
    }
}
