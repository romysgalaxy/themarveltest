<?php

namespace App\Controller;

use App\Model\HeroModel;
use App\Model\HeroPowerModel;

class HomeController extends AbstractController {

    /**
     * Affiche la page d'accueil
     */
    public function index() {

        $heroModel = new HeroModel();
        
        $team1 = $heroModel->findbyTeam(1);
        $team2 = $heroModel->findByTeam(2);
        $team3 = $heroModel->findByTeam(3);

        $this->render('home', [
            'team1' => $team1,
            'team2' => $team2,
            'team3' => $team3,
        ]);
    }

    /**
     * Affiche chaque page d'un héro
     */
    public function show()
    {
        /** Récupère le paramètre GET */
        $id = $_GET['id'];
        $heroModel = new HeroModel();
        $hero = $heroModel->findOne($_GET['id']);
        
        $heroPowerModel = new HeroPowerModel();
        $heroPower = $heroPowerModel->findPower($id);

        $this->render('show', [
            'hero' => $hero,
            'power' => $heroPower
        ]);
    }
}