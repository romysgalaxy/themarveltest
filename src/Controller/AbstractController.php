<?php

namespace App\Controller;

class AbstractController {

    /**
     * Affiche une vue
     * 
     * @param string $path Chemin de la vue
     * @param array[] $params Paramètres de la vue
     * @return void
     */
    protected function render(string $path, array $params = []): void 
    {
        require VIEW . $path . '.php'; 
    }
}