<?php

namespace App\Controller;

use App\Model\PowerModel;
use App\Model\TeamModel;
use App\Model\HeroModel;

class AdminController extends AbstractController
{

    /**
     * Affiche la page d'administration
     */
    public function index()
    {
        $heroModel = new HeroModel();
        $heros = $heroModel->findAll();

        $this->render('admin/admin', [
            'heros' => $heros
        ]);
    }

    public function create()
    {
        /** En cas de POST, mise à jour de l'entité */
        if (isset($_POST) && !empty($_POST)) {
            /** Instanciation du héros */
            $heroModel = new HeroModel();
            $heroModel->create(
                $_POST['lastname'],
                $_POST['firstname'],
                $_POST['identity'],
                $_POST['picture'],
                $_POST['origin'],
                $_POST['description'],
                $_POST['team'],
            );
            header('Location: /admin');
        }

        $powerModel = new PowerModel();
        $powerModel = $powerModel->findAll();
        $teamModel = new TeamModel();
        $teamModel = $teamModel->findAll();

        $this->render('form/CreateForm', [
            'powerModel' => $powerModel,
            'teamModel' => $teamModel
        ]);
    }

    public function edit()
    {
        /** Récupère le paramètre GET */
        if (isset($_GET['id'])) {
            /** Instanciation du héros */
            $heroModel = new HeroModel();

            /** En cas de POST, mise à jour de l'entité */
            if (isset($_POST) && !empty($_POST)) {
                $storedHero = $heroModel->findOne($_GET['id']);
                if (isset($_POST['identity']) && $storedHero->getIdentity() !== $_POST['identity']) {
                    $heroModel->update($storedHero->getId(), 'identity', $_POST['identity']);
                }
                if (isset($_POST['firstname']) && $storedHero->getFirstname() !== $_POST['firstname']) {
                    $heroModel->update($storedHero->getId(), 'firstname', $_POST['firstname']);
                }
                if (isset($_POST['lastname']) && $storedHero->getLastname() !== $_POST['lastname']) {
                    $heroModel->update($storedHero->getId(), 'lastname', $_POST['lastname']);
                }
                if (isset($_POST['origin']) && $storedHero->getOrigin() !== $_POST['origin']) {
                    $heroModel->update($storedHero->getId(), 'origin', $_POST['origin']);
                }
                if (isset($_POST['description']) && $storedHero->getDescription() !== $_POST['description']) {
                    $heroModel->update($storedHero->getId(), 'description', $_POST['description']);
                }
                if (isset($_POST['team']) && $storedHero->getTeam() !== $_POST['team']) {
                    $heroModel->update($storedHero->getId(), 'team', $_POST['team']);
                }
            }

            /** Récupère le héro */
            $hero = $heroModel->findOne($_GET['id']);

            $powerModel = new PowerModel();
            $powerModel = $powerModel->findAll();
            $teamModel = new TeamModel();
            $teamModel = $teamModel->findAll();

            $this->render('form/UpdateForm', [
                'powerModel' => $powerModel,
                'teamModel' => $teamModel,
                'hero' => $hero
            ]);
        }
    }

    public function delete()
    {
        /** Récupère le paramètre GET */
        if (isset($_GET['id'])) {
            /** Instanciation du héros */
            $heroModel = new HeroModel();
            $heroModel->delete($_GET['id']);
            header('Location: /admin');
        }
    }
}
