<?php

namespace App\Service;

class Router {

    /**
     * Fait correspondre une URL à une route et execute un callback
     */
    public function match() {
        $routes = ROUTES;
        foreach ($routes as $route) {
            if ($_SERVER['REQUEST_URI'] === $route[1]) {
                $class = explode('#', $route[2]);
                return call_user_func([new $class[0], $class[1]]);
            } 
        } 
        return false;
    }
}
