<?php 

namespace App\Service;
use \PDO;

/**
 * @property string $dsn
 * @property string $user
 * @property string $password
 * @property ?PDO $db
 */
class Database {
    
    /** @var string */
    private $dsn;

    /** @var string */
    private $user;

    /** @var string */
    private $password;

    /** @var ?PDO */
    public $db;

    /**
     * Récupère l'instance de PDO
     */
    public function getPDO(): ?PDO
    {
        $this->dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST .';port=3306';
        $this->user = DB_USER;
        $this->password = DB_PASSWORD;
        $this->db = new PDO($this->dsn, $this->user, $this->password);

        return $this->db;
    }
}