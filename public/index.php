<?php

require_once "../config.php";
require_once "../vendor/autoload.php";
require_once "../view/layout/header.php";

use App\Service\Router;

// Instancie le router
$router = new Router();

if($router->match() === false) {
    require_once "../view/404.php";
}

require_once "../view/layout/footer.php";