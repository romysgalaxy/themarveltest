<div class="container-lg mt-4">
    <h1 class="mb-5 text-white">Créer le héros</h1>
    <div class="mb-5">
        <a href="/admin" class="btn btn-danger">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
            </svg>
            Administration
        </a>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="" method="POST" name="marveltype" class="col-12">
                <div class="mb-3">
                    <label for="identity" class="form-label">Identity</label>
                    <input type="text" class="form-control" name="identity" aria-describedby="identity">
                    <div id="identity" class="form-text">Super hero name</div>
                </div>
                <div class="mb-3">
                    <label for="picture" class="form-label">Picture</label>
                    <input type="file" class="form-control" name="picture" id="picture">
                </div>
                <div class="mb-3">
                    <label for="firstname" class="form-label">Firstname</label>
                    <input type="text" class="form-control" name="firstname" aria-describedby="firstname">
                    <div id="firstname" class="form-text">Civil hero name</div>
                </div>
                <div class="mb-3">
                    <label for="lastname" class="form-label">Lastname</label>
                    <input type="text" class="form-control" name="lastname">
                </div>
                <div class="mb-3">
                    <legend>Team :</legend>
                    <div class="d-flex row">
                        <?php foreach ($params['teamModel'] as $teamModel) : ?>
                            <div class="form-check col-12 col-sm-4">
                                <input class="form-check-input" type="radio" name="team" value="<?= $teamModel->getId() ?>">
                                <label class="form-check-label" for="team">
                                    <?= $teamModel->getName() ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="origin" class="form-label">Origin</label>
                    <input type="text" class="form-control" name="origin">
                </div>
                <div class="mb-3">
                    <legend>Power(s) :</legend>
                    <div class="d-flex row">
                        <?php foreach ($params['powerModel'] as $powerModel) : ?>
                            <div class="form-check col-12 col-lg-3 col-sm-6">
                                <input class="form-check-input" type="checkbox" name="power">
                                <label class="form-check-label">
                                    <?= $powerModel->getPower() ?>
                                </label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="mb-2" for="description">Description</label>
                    <div class="form-floating">
                        <textarea class="form-control" name="description" style="height: 200px">
                        </textarea>
                    </div>
                </div>
                <input type="submit" value="Enregistrer" class="btn btn-danger my-2"></button>
            </form>
        </div>
    </div>
</div>