<div class="marvel-identity d-flex justify-content-center align-items-center">
    <p class="mb-0"><?= $params['hero']->getIdentity() ?></p>
</div>
<div class="">
    <div class="row justify-content-center mt-5">
        <div class="col-4">
            <div class="marvel-card">
                <img src="<?= SERVER ?>asset/img/hero/<?= $params['hero']->getPicture() ?>" alt="super hero picture" width="100%">
            </div>
        </div>
        <div class="col-6">
            <div class="marvel-data text-white p-2">
                <div class="d-flex">
                    <p class="fw-bold">Firstname:</p>
                    <p class="ms-2 mb-0"><?= $params['hero']->getFirstname() ?></p>
                </div>
                <div class="d-flex">
                    <p class="fw-bold">Lastname:</p>
                    <p class="ms-2 mb-0"><?= $params['hero']->getLastname() ?></p>
                </div>
                <div class="d-flex">
                    <p class="fw-bold">Team:</p>
                    <p class="ms-2 mb-0"><?= $params['hero']->getTeam() ?></p>
                </div>
                <div class="d-flex">
                    <p class="fw-bold">Origin:</p>
                    <p class="ms-2 mb-0"><?= $params['hero']->getOrigin() ?></p>
                </div>
                <div class="d-flex my-2">
                    <p class="fw-bold mb-0">Powers:</p>
                    <?php foreach($params['power'] as $power): ?>
                        <p style="background: red; border-radius: 4px; padding: 4px; text-align: center" class="ms-2 mb-0"><?= $power->power ?></p>
                    <?php endforeach; ?>
                </div>
                <div>
                    <p style="font-weight: bold;">Description:</p>
                    <p class="mb-0">
                        <?= $params['hero']->getDescription() ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>