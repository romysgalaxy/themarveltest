<div class="container-lg">
    <h2 class="title-team text-white mb-4 mt-5">TEAM #1 Avengers<h2>
    <div class="d-flex justify-content-center">
        <div class="row">
            <?php foreach ($params['team1'] as $hero) : ?>
                <div class="col-12 col-md-4 mb-4">
                    <div class="card marvel-card">
                        <img src="<?= SERVER ?>asset/img/hero/<?= $hero->getPicture() ?>" alt="super hero picture" width="100%">
                        <a href="<?= SERVER . 'show?id=' . $hero->getId() ?>" class="d-block"><?= $hero->getIdentity() ?></a>
                        <div class="trapeze">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <h2 class="title-team text-white mb-4 mt-5">TEAM #2 Guardians of the Galaxy</h2>
    <div class="d-flex justify-content-center">
        <div class="row">
            <?php foreach ($params['team2'] as $hero) : ?>
                <div class="col-12 col-md-4 mb-4">
                    <div class="card marvel-card">
                        <img src="<?= SERVER ?>asset/img/hero/<?= $hero->getPicture() ?>" alt="super hero picture" width="100%">
                        <a href="<?= SERVER . 'show?id=' . $hero->getId() ?>" class="d-block"><?= $hero->getIdentity() ?></a>
                        <div class="trapeze">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <h2 class="title-team text-white mb-4 mt-5">TEAM #3 X-Men</h2>
    <div class="d-flex justify-content-center">
        <div class="row">
            <?php foreach ($params['team3'] as $hero) : ?>
                <div class="col-12 col-md-4 mb-4">
                    <div class="card marvel-card">
                        <img src="<?= SERVER ?>asset/img/hero/<?= $hero->getPicture() ?>" alt="super hero picture" width="100%">
                        <a href="<?= SERVER . 'show?id=' . $hero->getId() ?>" class="d-block"><?= $hero->getIdentity() ?></a>
                        <div class="trapeze">
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>