<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>The Marvel Test</title>
    <link href="<?= SERVER ?>asset/css/style.css" rel="stylesheet">
    <link href="<?= SERVER ?>asset/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-sm nav-background navbar-dark">
        <div class="container-fluid">
            <a href="/">
                <img src="<?= SERVER ?>asset/img/logo.png" alt="logo marvel" height="50px">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-4">
                    <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Repository</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/admin">Admin</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>