<?php

/** Répertoire racine du serveur */
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
/** Répertoire des vues */
define('VIEW', dirname(ROOT) . '/view/');
/** Nom du serveur */
define('SERVER', '/');
/** Récupère les paramètres en $_GET */
define('QUERYSTRING', $_SERVER['QUERY_STRING']);

/** Connexion à la base de donnée */
const DB_HOST = 'themarveltest_mysql8';
const DB_NAME = 'themarveltest';
const DB_USER = 'root';
const DB_PASSWORD = 'root';

/** Enregistrement des routes */
const ROUTES = [
    "home" => ["home", "/", "App\Controller\HomeController#index"],
    "show" => ["show", '/show?' . QUERYSTRING, "App\Controller\HomeController#show"],
    "admin" => ["admin", "/admin", "App\Controller\AdminController#index"],
    "admin_create" => ["admin_create", '/admin/create', "App\Controller\AdminController#create"],
    "admin_edit" => ["admin_edit", '/admin/edit?' . QUERYSTRING, "App\Controller\AdminController#edit"],
    "admin_delete" => ["admin_delete", '/admin/delete?' . QUERYSTRING, "App\Controller\AdminController#delete"],
];